module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        dark: {
          elem: '#2B3945',
          bg: '#202C37',
          txt: '#FFFFFF',
          link: '#81E6D9'
        },
        light: {
          elem: '#FFFFFF',
          bg: '#FAFAFA',
          txt: '#111517',
          link: '#2B6CB0',
          input: '#858585',
        }
      }
    },
    variants: {
      extend: {},
    },
    plugins: [],
  }
}
