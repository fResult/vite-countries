import Axios from 'axios'

const baseURL = 'https://restcountries.eu/rest/v2'
const timeout = 1000

const axios = Axios.create({
  baseURL,
  timeout
})

export default axios
