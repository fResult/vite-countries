import React from 'react'

function LoadingIndicator() {
  return (
    <div className="w-full h-full fixed block top-0 left-0 bg-white opacity-75 z-50">
      <span
        className="text-light-text dark:text-dark-text opacity-75 top-1/2 my-0 mx-auto block relative w-0 h-0"
        style={{ top: '50%' }}
      >
        <svg className="animate-spin" viewBox="0 0 72 72" />
      </span>
    </div>
  )
}

export default LoadingIndicator
