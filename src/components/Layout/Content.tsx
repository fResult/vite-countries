import React from 'react'

type ContentProps = {
  children: React.ReactNode
}

const Content = ({ children }: ContentProps) => {
  return (
    <article className="content py-8 sm:py-10 px-4 sm:px-20">
      {children}
    </article>
  )
}

export default Content
