import React from 'react'
import Content from './Content'
import Header from './Header'
import Footer from "./Footer";

type LayoutProps = {
  children: React.ReactNode
}

const Layout = ({ children }: LayoutProps) => {
  return (
    <div className="layout min-h-screen">
      <Header />
      <Content>{children}</Content>
      <Footer/>
    </div>
  )
}

export default Layout
