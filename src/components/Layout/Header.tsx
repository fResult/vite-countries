import React, { useContext } from 'react'
import { ThemeContext } from '../Context/ThemeContext'

const Header = () => {
  const { theme, setTheme } = useContext(ThemeContext)
  const isDark = theme === 'dark'

  return (
    <div
      className="header px-4 sm:px-20 shadow-2xl flex items-center justify-between"
      style={{ height: 80 }}
    >
      Header
      <span
        onClick={() => setTheme && setTheme(isDark ? 'light' : 'dark')}
        className="cursor-pointer"
      >
        {isDark ? 'Light' : 'Dark'}&nbsp;Mode
      </span>
    </div>
  )
}

export default Header
