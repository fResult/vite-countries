import React from 'react'

type CardProps<T> = {
  className?: string
  style?: React.CSSProperties
  children: React.ReactNode
}

function Card<T>(props: CardProps<T>) {
  const { className, style, children } = props

  return (
    <div
      className={`card ${
        className ? className : ''
      } shadow-lg bg-light-elem dark:bg-dark-elem rounded-lg overflow-hidden`}
      style={style}
    >
      {children}
    </div>
  )
}

export default Card
