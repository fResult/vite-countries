import React, { useState } from 'react'
import './App.css'
import Layout from './components/Layout'
import Countries from "./pages/Countries";

function App() {
  return (
    <div className="text-light-txt dark:text-dark-txt bg-light-bg dark:bg-dark-bg">
      <Layout>
        <Countries />
      </Layout>
    </div>
  )
}

export default App
