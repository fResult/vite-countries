import React, { useEffect, useState } from 'react'
import * as apiCountries from '../../services/countries.api'
import Card from '../../components/Card'
import LoadingIndicator from '../../components/LoadingIndicator'

const Countries = () => {
  const [countries, setCountries] = useState<Array<Country>>([])
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState('')
  useEffect(() => {
    ;(async () => {
      try {
        setIsLoading(true)
        const { data } = await apiCountries.getAll()
        setCountries(data)
      } catch (err) {
        console.error('❌ Error', err.message)
        setError(err.response ? err.response.data.message : err.message)
      } finally {
        setIsLoading(false)
      }
    })()
  }, [])
  return (
    <div className="flex flex-wrap flex-row justify-center sm:justify-around gap-10">
      {isLoading ? (
        <LoadingIndicator />
      ) : !error ? (
        countries?.map((country) => {
          return (
            <div key={country.alpha3Code}>
              <Card
                className={`country-card cursor-pointer`}
                style={{ height: 335, width: 265 }}
              >
                <img
                  style={{ height: 160, width: 265, objectFit: 'cover' }}
                  src={country.flag}
                  alt={country.alpha3Code}
                />
                <div className="card-body py-6 px-4">
                  <div className="mb-4">
                    <span className="text-lg font-bold">{country.name}</span>
                  </div>
                  <div>
                    <span className="label">Population:&nbsp;</span>
                    <span>{country.population.toLocaleString()}</span>
                  </div>
                  <div>
                    <span className="label">Region:&nbsp;</span>
                    <span>{country.region}</span>
                  </div>
                  <div>
                    <span className="label">Capital:&nbsp;</span>
                    <span>{country.capital}</span>
                  </div>
                </div>
              </Card>
            </div>
          )
        })
      ) : (
        <div className="text-red-600 text-2xl">{error}</div>
      )}
    </div>
  )
}

export default Countries
