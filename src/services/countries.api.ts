import axios from '../config/axios.config'

// https://restcountries.eu/rest/v2/all?fields=name;capital;currencies
export async function getAll() {
  return await axios.get('/all?fields=name;nativeName;topLevelDomain;population;region;subRegion;languages;currencies;capital;borders;flag;alpha3Code')
}

export async function getById(id: string) {
  return await axios.get(`https://restcountries.eu/rest/v2/alpha/${id}`)
}
